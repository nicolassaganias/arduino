## introducción al arduino
## instalación arduino IDE, driver CH340 y primeros pasos

cuando hablamos de arduino estamos hablando de una marca. pero la mayoría de los "arduinos" que existen en el mercado son copias. que sean copias no quiere decir que sean ilegales o ilegítimas. arduino es una marca pero también es una comunidad que ha desarrollado tanto una placa de desarrollo como un software libres. muchos desarrolladores independientes han construido nuevas placas en base a estas ideas creadas por arduino. 
por eso, para utilizar la mayoría de las placas que hoy en el día se venden como arduino hace falta instalar, además del arduino IDE (el software para programar la placa) un driver adicional, el driver CH340.

más info: https://www.geekfactory.mx/tutoriales/tutoriales-arduino/driver-ch340-para-arduino-chinos-o-genericos/

## PASOS PARA LA INSTALACIÓN

1. descargar el archivo instalacion-arduino.zip
2. descomprimir el archivo instalacion-arduino.zip
3. abrir la carpeta /driver-ch340
4. instalar el driver según tu sistema operativo
5. abrir carpeta arduino-ide
6. instalar arduino según tu sistema operativo


